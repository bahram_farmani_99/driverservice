package Service;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity/*
@Table(name = "T_DRIVING")*/
@Cache(usage= CacheConcurrencyStrategy.READ_WRITE)
public class DriverModel {
    @Id
    private int c_id;
    private int c_active;
    private int c_vehicle;
    private String c_driver;

    public int getC_id() {
        return c_id;
    }

    public void setC_id(int c_id) {
        this.c_id = c_id;
    }

    public int getC_active() {
        return c_active;
    }

    public void setC_active(int c_active) {
        this.c_active = c_active;
    }

    public int getC_vehicle() {
        return c_vehicle;
    }

    public void setC_vehicle(int c_vehicle) {
        this.c_vehicle = c_vehicle;
    }

    public String getC_driver() {
        return c_driver;
    }

    public void setC_driver(String c_driver) {
        this.c_driver = c_driver;
    }
}

