package Service;

import org.springframework.stereotype.Repository;

import javax.persistence.*;


public class DriverDao {

    @PersistenceContext
    public EntityManagerFactory entityManagerFactory;

    public void save(DriverModel driver){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction tran  = entityManager.getTransaction();
        tran.begin();
        entityManager.persist(driver);
        tran.commit();
        entityManager.close();
    }
}
