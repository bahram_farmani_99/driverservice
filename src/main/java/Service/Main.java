package Service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

public class Main {
    public static void main(String[] args) {
      /*  ApplicationContext apt = new AnnotationConfigApplicationContext(Config.class);

        EntityManagerFactory factory =apt.getBean(EntityManagerFactory.class);
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction tran = entityManager.getTransaction();
        tran.begin();*/

        SessionFactory sf = new Configuration().configure("/src/main/hibernate.cfg.xml").buildSessionFactory();
        Session session = sf.openSession();
        Transaction tran = session.beginTransaction();

        DriverModel driver = new DriverModel();
        driver.setC_id(1);
        driver.setC_active(1);
        driver.setC_vehicle(3243);
        driver.setC_driver("674");

        session.save(driver);
        tran.commit();
        session.close();
    }

}
