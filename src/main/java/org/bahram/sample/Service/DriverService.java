package org.bahram.sample.Service;

import org.bahram.sample.Config;
import org.bahram.sample.Dao.DriverDao;
import org.bahram.sample.Entity.Driver;
import org.bahram.sample.Entity.Drivercards;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("DriverService")
public class DriverService {

    @Autowired
    public DriverDao driverDao;

    @Transactional
    public List<Driver> getDriver(){
       return driverDao.getalldriver();
    }

    @Transactional
    public void newdriver(Driver d){
        driverDao.newdriver(d);
    }

    @Transactional
    public void newdrivercards(Drivercards dc){
        driverDao.newdrivercards(dc);
    }
}
