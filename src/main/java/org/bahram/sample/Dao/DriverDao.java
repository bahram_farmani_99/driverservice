package org.bahram.sample.Dao;

import org.bahram.sample.Entity.Driver;
import org.bahram.sample.Entity.Drivercards;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DriverDao {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Driver> getalldriver() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        AbstractQuery<Driver> cq1 = cb.createQuery(Driver.class);
        Root<Driver> div = cq1.from(Driver.class);

        cq1.where(cb.greaterThan(div.<Comparable>get("active"), 0), cb.like(div.<String>get("driver"), "6%"));


        CriteriaQuery<Driver> select = ((CriteriaQuery<Driver>) cq1).select(div);
        TypedQuery<Driver> q = entityManager.createQuery(select);
        List<Driver> list = q.getResultList();
        return list;
    }

    public void newdriver(Driver d) {
        entityManager.persist(d);
    }

    public void newdrivercards(Drivercards dc) {
        entityManager.persist(dc);
    }

    /*  ((/get data wigh criteria and session/))

        public List<Driver> getAllDriver() {
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();

            Criteria criteria = session.createCriteria(Driver.class);
            Criterion c1 = Restrictions.eq("c_active",1);
            Criterion c2 = Restrictions.like("c_driver","6%");

            criteria.add(c1).add(c2);

            List<Driver> li = criteria.list();
            transaction.commit();
            session.close();
            return li;
        }
    */
}
