package org.bahram.sample.Controller;

import org.bahram.sample.Entity.Driver;
import org.bahram.sample.Entity.Drivercards;
import org.bahram.sample.Service.DriverService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("driver")
public class Controller {

    DriverService service = new DriverService();
/*
    @GET
    @Path("/salam")
    @Produces(MediaType.TEXT_PLAIN)
    public String slam(){
        return "salam";
    }*/

    @GET
    @Path("/getdata")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Driver> getdriver(){
       return service.getDriver();
    }

    @POST
    @Path("/setdata")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void postmethod(Drivercards dc) {
        service.newdrivercards(dc);
    }

}
