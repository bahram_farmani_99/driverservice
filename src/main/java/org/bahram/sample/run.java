package org.bahram.sample;

import org.bahram.sample.Entity.Driver;
import org.bahram.sample.Entity.Drivercards;
import org.bahram.sample.Service.DriverService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class run {
    public static void main(String[] args) {
        ApplicationContext appt = new AnnotationConfigApplicationContext(Config.class);
        DriverService service = appt.getBean(DriverService.class);

        Drivercards dc = new Drivercards();
        List<Driver> li = service.getDriver();

        for (Driver item : li) {
            System.out.println(item.getDriver());
            //get data from driver and insert into drivercards
            dc.setCardid(item.getId());
            dc.setCardactive(item.getActive());
            dc.setCardvehicle(item.getVehicle());
            dc.setCarddriver(item.getDriver());
        }
        service.newdrivercards(dc);
    }
}
