package org.bahram.sample.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Drivercards {
    @Id
    private int cardid;
    private int cardactive;
    private int cardvehicle;
    private String carddriver;

    public int getCardid() {
        return cardid;
    }

    public void setCardid(int cardid) {
        this.cardid = cardid;
    }

    public int getCardactive() {
        return cardactive;
    }

    public void setCardactive(int cardactive) {
        this.cardactive = cardactive;
    }

    public int getCardvehicle() {
        return cardvehicle;
    }

    public void setCardvehicle(int cardvehicle) {
        this.cardvehicle = cardvehicle;
    }

    public String getCarddriver() {
        return carddriver;
    }

    public void setCarddriver(String carddriver) {
        this.carddriver = carddriver;
    }
}
